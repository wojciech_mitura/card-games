export default class NextPlayOff {
    isWiner(
        winer,
        loser,
        whoWin,
        whoLose,
        playOffSetTimeOutAddAnimation,
        playOffSetTimeOutRemoveAnimation
    ) {
        let whichWinningCard = winer.childElementCount - 1
        let whichLostCard = loser.childElementCount - 1

        isWinerAddAnimation(
            winer,
            loser,
            whoWin,
            whichWinningCard,
            whichLostCard,
            playOffSetTimeOutAddAnimation
        )
        isWinerRemoveAnimation(
            winer,
            loser,
            whoWin,
            whoLose,
            whichWinningCard,
            whichLostCard,
            playOffSetTimeOutRemoveAnimation
        )
    }
}

function isWinerAddAnimation(
    winer,
    loser,
    whoWin,
    whichWinningCard,
    whichLostCard,
    playOffSetTimeOutAddAnimation
    ) {
    setTimeout(()=> {
        winer.children[whichWinningCard - 3].classList.add('animation-nextPlayOff-passive-card-goes-back-' + whoWin)
        winer.children[whichWinningCard - 4].classList.add('animation-nextPlayOff-active-card-goes-back-' + whoWin)
        loser.children[whichLostCard - 3].classList.add('animation-nextPlayOff-passive-card-goes-to-winer-' + whoWin)
        loser.children[whichLostCard - 4].classList.add('animation-nextPlayOff-active-card-goes-to-winer-' + whoWin)
    },playOffSetTimeOutAddAnimation)
}

function isWinerRemoveAnimation(
    winer,
    loser,
    whoWin,
    whoLose,
    whichWinningCard,
    whichLostCard,
    playOffSetTimeOutRemoveAnimation
    ) {
    setTimeout(()=> {
        winer.children[whichWinningCard - 3].classList.remove('animation-nextPlayOff-passive-card-on-table-' + whoWin, 'animation-nextPlayOff-passive-card-goes-back-' + whoWin, 'nextPlayOff-passive-card-on-table-' + whoWin)
        loser.children[whichLostCard - 3].classList.remove('animation-nextPlayOff-passive-card-on-table-' + whoLose, 'animation-nextPlayOff-passive-card-goes-to-winer-' + whoWin, 'nextPlayOff-passive-card-on-table-' + whoLose)
        winer.children[whichWinningCard - 4].classList.remove('animation-nextPlayOff-active-card-on-table-' + whoWin , 'animation-nextPlayOff-active-card-goes-back-' + whoWin, 'nextPlayOff-active-card-on-table-' + whoWin)
        winer.children[whichWinningCard - 4].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        loser.children[whichLostCard - 4].classList.remove('animation-nextPlayOff-active-card-on-table-' + whoLose, 'animation-nextPlayOff-active-card-goes-to-winer-' + whoWin, 'nextPlayOff-active-card-on-table-' + whoLose)
        loser.children[whichLostCard - 4].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        winer.append(loser.children[whichLostCard - 3], loser.children[whichLostCard - 4])
    },playOffSetTimeOutRemoveAnimation)
}