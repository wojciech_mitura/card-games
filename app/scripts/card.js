export default class Card {
    constructor(
        type,
        color,
        points,
        img,
        id
    ) {
        this.type = type,
        this.color = color,
        this.points = points,
        this.img = img,
        this.id = id
    }
}