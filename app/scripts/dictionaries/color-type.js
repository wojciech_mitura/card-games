export const colorType = Object.freeze({
    wino: 'Pik',
    czerwo: 'Kier',
    żołądź: 'Trefl',
    dzwonek: 'Karo'
})