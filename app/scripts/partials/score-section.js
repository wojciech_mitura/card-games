const fightButton = document.querySelector('.game-section-fight-button');
const scoreSection = document.getElementById('score-section')
const scoreText = document.getElementById('score-section-text')

export default class ScoreSection {
    checksHowManyCards(player, opponent, whichGame) {
        if (player.children.length <= whichGame) {
            this.openSection('Lose');
            return true
        } else if (opponent.children.length <= whichGame) {
            this.openSection('Win');
            return true
        } else {
            return;
        }
    }

    openSection(score) {
        scoreSection.classList.add('score-section-active')
        fightButton.classList.remove('game-section-fight-button-visible')
        this.finalText(score)
    }

    finalText(score) {
        setTimeout(()=> {
            scoreText.innerText = 'You ' + score
        }, 1000)
    }
}