import DealCards from '../deal-cards.js';

const body = document.getElementById('body');
const startingSection = document.getElementById('starting-section');
const name = document.forms['starting-section-form']['starting-section-name'];
const nameError = document.getElementById('Error-name');
const checkButton = document.getElementById('starting-section-button');

const dealCards = new DealCards();
export default class StartGame {
    nameCheck() {
        body.addEventListener('click', function goToGame(e) {
            const targerClick = e.target;
            event.preventDefault();
            nameLengthUpperLimit();
            nameLengthLowerLimit(goToGame, targerClick);
        });
    }
}

function goToGameSection() {
    startingSection.classList.add('starting-section-hide')
    dealCards.createCards(name.value)
}

function nameLengthUpperLimit() {
    name.addEventListener('keyup', (event)=> {
        if (name.value.length >= 10) {
            nameError.classList.add('starting-section-name-error-visibile')
            nameError.innerText = 'you can use maximum ten characters'
            name.value = name.value.substr(0, 10)
        }
        else {
            nameError.classList.remove('starting-section-name-error-visibile')
        }
    })
}

function nameLengthLowerLimit(goToGame, targerClick) {
    if (targerClick == checkButton && name.value.length < 3) {
        name.classList.remove('starting-section-name-input-correct')
        name.classList.add('starting-section-name-input-error')
        nameError.classList.add('starting-section-name-error-visibile')
        nameError.innerText = 'Use minimum three characters'
    }

    else if (targerClick == checkButton && name.value.length >= 3) {
        body.removeEventListener('click', goToGame);
        name.classList.add('starting-section-name-input-correct')
        nameError.classList.remove('starting-section-name-error-visibile')
        startingSection.classList.add('starting-section-animation')
        setTimeout(()=> {
            goToGameSection()
        }, 500);
    }
    else if (name.value.length >= 3) {
        name.classList.add('starting-section-name-input-correct')
        nameError.classList.remove('starting-section-name-error-visibile')
    }
}
