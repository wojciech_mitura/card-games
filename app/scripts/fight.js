import PlayOff from './play-off.js'
import {scoreValue as scoreValue} from './play-off.js'
import ScoreSection from './partials/score-section.js'

export default class Fight {
    constructor(){
        this.playOff = new PlayOff()
        this.scoreSection = new ScoreSection()
    }
    
    cards(players, playerCards, opponentCards, listeningButton, playerScore, opponentScore) {
        let player = players[0];
        let opponent = players[1];
        let activeOpponentCards = [];
        let activePlayerCards = [];
        let playOffCards = [];
        let whichCards = 0
        let playOffCardPosition = 2
        let whenPlay = 'playOff'
        
        let playOffSetTimeOutAddAnimation = 2600
        let playOffSetTimeOutRemoveAnimation = 2950
        
        activePlayerCards = player.cards.splice(0, 1)[0];
        activeOpponentCards = opponent.cards.splice(0, 1)[0];

        playerScore.innerHTML = player.cards.length
        opponentScore.innerHTML = opponent.cards.length

        this.joiningActiveCard(activePlayerCards, activeOpponentCards)

        if (!this.scoreSection.checksHowManyCards(playerCards, opponentCards, 0)) {
            this.playerCardOnTable(playerCards, opponentCards, activePlayerCards.img, activeOpponentCards.img)

            if (activePlayerCards.points > activeOpponentCards.points) {
                this.playCardBackToHand(playerCards, 'player', opponentCards)
                this.isWiner(playerCards, opponentCards, player.cards, opponent.cards)
                listeningButton = 'fight'

                player.cards.push(...this.allActiveCards)

            } else if (activePlayerCards.points < activeOpponentCards.points) {
                this.playCardBackToHand(opponentCards, 'opponent', playerCards)
                this.isWiner(playerCards, opponentCards, player.cards, opponent.cards)
                listeningButton = 'fight'
                
                opponent.cards.push(...this.allActiveCards)
            } 
            else {   
                playOffCards.push(this.allActiveCards)
                //* tu odpala playoff*/
                this.playOff.play(
                    player.cards,
                    opponent.cards,
                    ...playOffCards,
                    playerCards,
                    opponentCards,
                    whichCards,
                    playOffSetTimeOutAddAnimation,
                    playOffSetTimeOutRemoveAnimation,
                    playOffCardPosition,
                    whenPlay,
                    playerScore,
                    opponentScore,
                    listeningButton
                )
                listeningButton = 'playOff'    
            }
            return listeningButton
        }
    }

    joiningActiveCard(playerCard, opponentCard) {
        this.allActiveCards = [playerCard, opponentCard];
    }

    playerCardOnTable(player, opponent, playerCardImg, opponentCardImg) {
        let playerCard = player.lastChild
        let opponentCard = opponent.lastChild

        playerCard.classList.add('animation-card-on-table-player')
        playerCard.style.background = 'url(' + playerCardImg + ') no-repeat center/cover';
        opponentCard.classList.add('animation-card-on-table-opponent')
        opponentCard.style.background = 'url(' + opponentCardImg + ') no-repeat center/cover';

        setTimeout(() => {
            playerCard.classList.add('player-card-on-table')
            opponentCard.classList.add('opponent-card-on-table')
            playerCard.classList.remove('animation-card-on-table-player')
            opponentCard.classList.remove('animation-card-on-table-opponent')
        }, 400);

        setTimeout(() => {
            playerCard.classList.remove('animation-card-player-win', 'animation-card-opponent-lose')
            opponentCard.classList.remove('animation-card-opponent-win', 'animation-card-player-lose')
        }, 1400);
    }

    isWiner(player, opponent, playerCards, opponentCards) {
        let playerCard = player.lastChild
        let opponentCard = opponent.lastChild
        setTimeout(() => {
            scoreValue(playerScore, opponentScore,  playerCards, opponentCards)
            playerCard.classList.remove('player-card-on-table')
            opponentCard.classList.remove('opponent-card-on-table')
            playerCard.style.background = 'url(../../images/revers.png) no-repeat center/ cover'
            opponentCard.style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        }, 1400);
    }

    playCardBackToHand(winPlayer, player, losePlayer) {
        let winCard = winPlayer.lastChild
        let loseCard = losePlayer.lastChild
        setTimeout(() => {
            winCard.classList.add('animation-card-' + player + '-win')
            loseCard.classList.add('animation-card-' + player + '-lose')
        }, 1000);
        setTimeout(() => {
            winPlayer.appendChild(loseCard)
        }, 1400);
    }
}