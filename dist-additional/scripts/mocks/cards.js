import Card from  '../card.js';

import { cardTypes } from '../dictionaries/card-type.js'
import { colorType } from '../dictionaries/color-type.js'

export const cards = [
    new Card(
        cardTypes.as,
        colorType.czerwo,
        14,
        '.././images/kier/as.png',
        52
    ),
    new Card(
        cardTypes.as,
        colorType.wino,
        14,
        '.././images/pik/as.png',
        51
    ),
    new Card(
        cardTypes.as,
        colorType.dzwonek,
        14,
        '.././images/karo/as.png',
        50
    ),
    new Card(
        cardTypes.as,
        colorType.żołądź,
        14,
        '.././images/trefl/as.png',
        49
    ),
    new Card(
        cardTypes.krol,
        colorType.czerwo,
        13,
        '.././images/kier/krol.png',
        48
    ),
    new Card(
        cardTypes.krol,
        colorType.wino,
        13,
        '.././images/pik/krol.png',
        47
    ),
    new Card(
        cardTypes.krol,
        colorType.dzwonek,
        13,
        '.././images/karo/krol.png',
        46
    ),
    new Card(
        cardTypes.krol,
        colorType.żołądź,
        13,
        '.././images/trefl/krol.png',
        45
    ),
    new Card(
        cardTypes.dama,
        colorType.czerwo,
        12,
        '.././images/kier/dama.png',
        44
    ),
    new Card(
        cardTypes.dama,
        colorType.wino,
        12,
        '.././images/pik/dama.png',
        43
    ),
    new Card(
        cardTypes.dama,
        colorType.dzwonek,
        12,
        '.././images/karo/dama.png',
        42
    ),
    new Card(
        cardTypes.dama,
        colorType.żołądź,
        12,
        '.././images/trefl/dama.png',
        41
    ),
    new Card(
        cardTypes.walet,
        colorType.czerwo,
        11,
        '.././images/kier/walet.png',
        40
    ),
    new Card(
        cardTypes.walet,
        colorType.wino,
        11,
        '.././images/pik/walet.png',
        39
    ),
    new Card(
        cardTypes.walet,
        colorType.dzwonek,
        11,
        '.././images/karo/walet.png',
        38
    ),
    new Card(
        cardTypes.walet,
        colorType.żołądź,
        11,
        '.././images/trefl/walet.png',
        37
    ),
    new Card(
        cardTypes.dziesięć,
        colorType.czerwo,
        10,
        '.././images/kier/10.png',
        36
    ),
    new Card(
        cardTypes.dziesięć,
        colorType.wino,
        10,
        '.././images/pik/10.png',
        35
    ),
    new Card(
        cardTypes.dziesięć,
        colorType.dzwonek,
        10,
        '.././images/karo/10.png',
        34
    ),
    new Card(
        cardTypes.dziesięć,
        colorType.żołądź,
        10,
        '.././images/trefl/10.png',
        33
    ),
    new Card(
        cardTypes.dziewięć,
        colorType.czerwo,
        9,
        '.././images/kier/9.png',
        32
    ),
    new Card(
        cardTypes.dziewięć,
        colorType.wino,
        9,
        '.././images/pik/9.png',
        31
    ),
    new Card(
        cardTypes.dziewięć,
        colorType.dzwonek,
        9,
        '.././images/karo/9.png',
        30
    ),
    new Card(
        cardTypes.dziewięć,
        colorType.żołądź,
        9,
        '.././images/trefl/9.png',
        29
    ),
    new Card(
        cardTypes.osiem,
        colorType.czerwo,
        8,
        '.././images/kier/8.png',
        28
    ),
    new Card(
        cardTypes.osiem,
        colorType.wino,
        8,
        '.././images/pik/8.png',
        27
    ),
    new Card(
        cardTypes.osiem,
        colorType.dzwonek,
        8,
        '.././images/karo/8.png',
        26
    ),
    new Card(
        cardTypes.osiem,
        colorType.żołądź,
        8,
        '.././images/trefl/8.png',
        25
    ),
    new Card(
        cardTypes.siedem,
        colorType.czerwo,
        7,
        '.././images/kier/7.png',
        24
    ),
    new Card(
        cardTypes.siedem,
        colorType.wino,
        7,
        '.././images/pik/7.png',
        23
    ),
    new Card(
        cardTypes.siedem,
        colorType.dzwonek,
        7,
        '.././images/karo/7.png',
        22
    ),
    new Card(
        cardTypes.siedem,
        colorType.żołądź,
        7,
        '.././images/trefl/7.png',
        21
    ),
    new Card(
        cardTypes.sześć,
        colorType.czerwo,
        6,
        '.././images/kier/6.png',
        20
    ),
    new Card(
        cardTypes.sześć,
        colorType.wino,
        6,
        '.././images/pik/6.png',
        19
    ),
    new Card(
        cardTypes.sześć,
        colorType.dzwonek,
        6,
        '.././images/karo/6.png',
        18
    ),
    new Card(
        cardTypes.sześć,
        colorType.żołądź,
        6,
        '.././images/trefl/6.png',
        17
    ),
    new Card(
        cardTypes.pięć,
        colorType.czerwo,
        5,
        '.././images/kier/5.png',
        16
    ),
    new Card(
        cardTypes.pięć,
        colorType.wino,
        5,
        '.././images/pik/5.png',
        15
    ),
    new Card(
        cardTypes.pięć,
        colorType.dzwonek,
        5,
        '.././images/karo/5.png',
        14
    ),
    new Card(
        cardTypes.pięć,
        colorType.żołądź,
        5,
        '.././images/trefl/5.png',
        13
    ),
    new Card(
        cardTypes.cztery,
        colorType.czerwo,
        4,
        '.././images/kier/4.png',
        12
    ),
    new Card(
        cardTypes.cztery,
        colorType.wino,
        4,
        '.././images/pik/4.png',
        11
    ),
    new Card(
        cardTypes.cztery,
        colorType.dzwonek,
        4,
        '.././images/karo/4.png',
        10
    ),
    new Card(
        cardTypes.cztery,
        colorType.żołądź,
        4,
        '.././images/trefl/4.png',
        9
    ),
    new Card(
        cardTypes.trzy,
        colorType.czerwo,
        3,
        '.././images/kier/3.png',
        8
    ),
    new Card(
        cardTypes.trzy,
        colorType.wino,
        3,
        '.././images/pik/3.png',
        7
    ),
    new Card(
        cardTypes.trzy,
        colorType.dzwonek,
        3,
        '.././images/karo/3.png',
        6
    ),
    new Card(
        cardTypes.trzy,
        colorType.żołądź,
        3,
        '.././images/trefl/3.png',
        5
    ),
    new Card(
        cardTypes.dwa,
        colorType.czerwo,
        2,
        '.././images/kier/2.png',
        4
    ),
    new Card(
        cardTypes.dwa,
        colorType.wino,
        2,
        '.././images/pik/2.png',
        3
    ),
    new Card(
        cardTypes.dwa,
        colorType.dzwonek,
        2,
        '.././images/karo/2.png',
        2
    ),
    new Card(
        cardTypes.dwa,
        colorType.żołądź,
        2,
        '.././images/trefl/2.png',
        1
    )
]