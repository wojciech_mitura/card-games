import NextPlayOff from './next-play-off.js'
import ScoreSection from './partials/score-section.js'

const background = document.getElementById('body')
export default class PlayOff {
    constructor() {
        this.nextPlayOff = new NextPlayOff();
        this.scoreSection = new ScoreSection();
    }

    play(
        playerCards,
        opponentCards,
        playOffCards, 
        playerAnimation, 
        opponentAnimation, 
        whichCards, 
        playOffSetTimeOutAddAnimation, 
        playOffSetTimeOutRemoveAnimation, 
        playOffCardPosition, 
        whenPlay, 
        playerScore, 
        opponentScore,
        listeningButton
    ) {
            
        let activePlayerCard;
        let activeOpponentCard;
        let nextPlayOffCards = 2
        let nextplayOffCardPosition = 4 
        let nextPlayOffSetTimeOutAddAnimation = 2500
        let nextPlayOffSetTimeOutRemoveAnimation = 2900
        
        activePlayerCard = playerCards.splice(whichCards, 2);
        activeOpponentCard = opponentCards.splice(whichCards, 2);

        playOffAddBackground()
        scoreValue(
            playerScore,
            opponentScore,
            playerCards,
            opponentCards
        )

        playOffCards = ([...playOffCards, ...activePlayerCard, ...activeOpponentCard])
        
        if(!this.scoreSection.checksHowManyCards(playerAnimation, opponentAnimation, 2)) {
            this.playOffCardOnTable(
            playerAnimation,
            opponentAnimation,
            activePlayerCard[1].img,
            activeOpponentCard[1].img,
            playOffCardPosition,
            whenPlay,
            playerCards,
            opponentCards
            )
        } else { 
        return;
    }

        if (activePlayerCard[1].points > activeOpponentCard[1].points) {
            playerCards.push(...playOffCards)
            this.playOffIsWiner(
                playerAnimation,
                opponentAnimation,
                'player',
                'opponent',
                playOffSetTimeOutAddAnimation,
                playOffSetTimeOutRemoveAnimation,
                playerCards,
                opponentCards,
                whenPlay   
            )
            playOffCards = [];

        } else if (activeOpponentCard[1].points > activePlayerCard[1].points) {
            playerCards.push(...playOffCards)
            this.playOffIsWiner(
                playerAnimation,
                opponentAnimation,
                'player',
                'opponent',
                playOffSetTimeOutAddAnimation,
                playOffSetTimeOutRemoveAnimation,
                playerCards,
                opponentCards,
                whenPlay   
            )
            playOffCards = [];

        } 
        else {
            setTimeout(()=> {
                this.play(
                    playerCards,
                    opponentCards,
                    playOffCards,
                    playerAnimation,
                    opponentAnimation,
                    nextPlayOffCards,
                    nextPlayOffSetTimeOutAddAnimation,
                    nextPlayOffSetTimeOutRemoveAnimation,
                    nextplayOffCardPosition,
                    'nextPlayOff',
                    playerCards,
                    opponentCards
                )
                playOffCards = [];
            },1600)
            listeningButton = 'nextPlayOff'
        }
        
    }

    playOffCardOnTable(
        player,
        opponent,
        playerCardImg,
        opponentCardImg,
        cardPosition,
        whenPlay
    ) {

        let whichPlayerCard = player.childElementCount - cardPosition
        let whichOpponentCard = opponent.childElementCount - cardPosition

        cardsOnTableAddAnimation(
            whichPlayerCard,
            whichOpponentCard,
            player,
            opponent,
            playerCardImg,
            opponentCardImg,
            whenPlay
        )
    }

    playOffIsWiner(
        winer,
        loser,
        whoWin,
        whoLose,
        playOffSetTimeOutAddAnimation,
        playOffSetTimeOutRemoveAnimation,
        playerCards,
        opponentCards,
        whenPlay
    ) {

        let whichWinningCard = winer.childElementCount - 1
        let whichLostCard = loser.childElementCount - 1

        isWinerAddAnimation(
            winer,
            loser,
            whichWinningCard,
            whichLostCard,
            whoWin,
            playOffSetTimeOutAddAnimation
        )

        isWinerremoveAnimation(winer,
            loser,
            whichWinningCard,
            whichLostCard,
            whoWin,
            whoLose,
            playerCards,
            opponentCards,
            playOffSetTimeOutRemoveAnimation
        )

        if(whenPlay === 'nextPlayOff') {
            this.nextPlayOff.isWiner(
                winer,
                loser,
                whoWin,
                whoLose,
                playOffSetTimeOutAddAnimation,
                playOffSetTimeOutRemoveAnimation
            )
        }
    }
}

function playOffAddBackground() {
    background.classList.add('background-play-off')
}

function playOffRemoveBackground() {
    setTimeout(()=> {
        background.classList.remove('background-play-off')
    }, 300)
}

function cardsOnTableAddAnimation(
    whichPlayerCard,
    whichOpponentCard,
    player,
    opponent,
    playerCardImg,
    opponentCardImg,
    whenPlay
    ) {
    setTimeout(()=> {
        player.children[whichPlayerCard].classList.add('animation-' + whenPlay + '-passive-card-on-table-player')
        opponent.children[whichOpponentCard].classList.add('animation-' + whenPlay + '-passive-card-on-table-opponent')
    },1000)

    setTimeout(()=> {
        player.children[whichPlayerCard].classList.add(whenPlay + '-passive-card-on-table-player')
        opponent.children[whichOpponentCard].classList.add(whenPlay + '-passive-card-on-table-opponent')
    },1300)

    setTimeout(()=> {
        player.children[whichPlayerCard - 1].classList.add('animation-' + whenPlay + '-active-card-on-table-player')
        player.children[whichPlayerCard - 1].style.background = 'url(' + playerCardImg + ') no-repeat center/cover';
        opponent.children[whichOpponentCard - 1].classList.add('animation-' + whenPlay + '-active-card-on-table-opponent')
        opponent.children[whichOpponentCard - 1].style.background = 'url(' + opponentCardImg + ') no-repeat center/cover';
    },1400)
    
    setTimeout(()=> {
        player.children[whichPlayerCard - 1].classList.add(whenPlay + '-active-card-on-table-player')
        opponent.children[whichOpponentCard - 1].classList.add(whenPlay + '-active-card-on-table-opponent')
    },1800)
}

function isWinerAddAnimation(
    winer,
    loser,
    whichWinningCard,
    whichLostCard,
    whoWin,
    playOffSetTimeOutAddAnimation
    ) {
    setTimeout(()=> {
        winer.children[whichWinningCard].classList.add('animation-card-' + whoWin + '-win')
        winer.children[whichWinningCard - 1].classList.add('animation-playOff-passive-card-goes-back-' + whoWin)
        winer.children[whichWinningCard - 2].classList.add('animation-playOff-active-card-goes-back-' + whoWin)
        loser.children[whichLostCard].classList.add('animation-card-' + whoWin + '-lose')
        loser.children[whichLostCard - 1].classList.add('animation-playOff-passive-card-goes-to-winer-' + whoWin)
        loser.children[whichLostCard - 2].classList.add('animation-playOff-active-card-goes-to-winer-' + whoWin)
    },playOffSetTimeOutAddAnimation)
}

function isWinerremoveAnimation(
    winer,
    loser,
    whichWinningCard,
    whichLostCard,
    whoWin,
    whoLose,
    playerCards,
    opponentCards,
    playOffSetTimeOutRemoveAnimation
    ) {
    setTimeout(()=> {
        winer.children[whichWinningCard].classList.remove('animation-card-on-table-' + whoWin, 'animation-card-' + whoWin + '-win', whoWin + '-card-on-table')
        winer.children[whichWinningCard].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        loser.children[whichLostCard].classList.remove('animation-card-on-table-' + whoLose, 'animation-card-' + whoWin + '-lose', whoLose + '-card-on-table')
        loser.children[whichLostCard].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        winer.children[whichWinningCard - 1].classList.remove('animation-playOff-passive-card-on-table-' + whoWin, 'animation-playOff-passive-card-goes-back-' + whoWin, 'playOff-passive-card-on-table-' + whoWin)
        loser.children[whichLostCard - 1].classList.remove('animation-playOff-passive-card-on-table-' + whoLose, 'animation-playOff-passive-card-goes-to-winer-' + whoWin, 'playOff-passive-card-on-table-' + whoLose)
        winer.children[whichWinningCard - 2].classList.remove('animation-playOff-active-card-on-table-' + whoWin , 'animation-playOff-active-card-goes-back-' + whoWin, 'playOff-active-card-on-table-' + whoWin)
        winer.children[whichWinningCard - 2].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        loser.children[whichLostCard - 2].classList.remove('animation-playOff-active-card-on-table-' + whoLose, 'animation-playOff-active-card-goes-to-winer-' + whoWin, 'playOff-active-card-on-table-' + whoLose)
        loser.children[whichLostCard - 2].style.background = 'url(../../images/revers.png) no-repeat center/ cover'
        winer.append(loser.children[whichLostCard], loser.children[whichLostCard - 1],  loser.children[whichLostCard - 2])
        playOffRemoveBackground()
        scoreValue(playerScore, opponentScore, playerCards, opponentCards)
    },playOffSetTimeOutRemoveAnimation)
}

export function scoreValue(playerScore, opponentScore, playerCards, opponentCards) {
    playerScore.innerHTML = playerCards.length
    opponentScore.innerHTML = opponentCards.length
}