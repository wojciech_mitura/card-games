import {cards as allCards} from './mocks/cards.js';
import Player from './player.js';
import GameSection from './partials/game-section.js' 

const listCardsBeforeDealing = document.getElementById('cards-before-dealing');

/**varables-animation*/
const playerActivecards = document.getElementById('player-active-cards')
const opponentActivecards = document.getElementById('opponent-active-cards')

const playerPlaceName = document.getElementById('player-place-name')
const sectionGamePlay = document.getElementById('game-section')

const playerScore = document.getElementById('playerScore')
const opponentScore = document.getElementById('opponentScore')

let countCards = 1;
let players = [];

export default class DealCards {
    constructor(){
        this.player = new Player();
        this.opponent = new Player('Opponent');
        this.goToGameSection = new GameSection();
    }

    createCards(playerName) {
        for (var i = 0; i < allCards.length; i++) {
            const cardLi = document.createElement('li');
            listCardsBeforeDealing.appendChild(cardLi);
            cardLi.classList.add('game-section-card-before-dealing')
        }
        this.dealCards(playerName)
    }

    dealCards(playerName) {
        let selectedCard;
        let allCardsNumber = allCards.length;

        sectionGamePlay.style.display = 'block';
        playerPlaceName.innerText = playerName;
        this.player.name = playerName;

        setTimeout(()=> {
            for (var i = 0; i < allCardsNumber; i++) {
                this.dealCardsToPlayers(selectedCard, i)
            }
        }, 1000)
        setTimeout(()=> {
            this.goToGameSection.fightButtonListener(players, playerActivecards, opponentActivecards, playerScore, opponentScore);
        },2000)
    }

    dealCardsToPlayers(selectedCard, i) {
        setTimeout(() => {
            let j = (Math.floor(Math.random() * allCards.length - 1));
            selectedCard = allCards.splice(j, 1)[0];
            countCards++;

            if (countCards % 2) {
                this.dealCardsToPlayersAnimation(selectedCard, this.player, playerActivecards, 'player', allCards.length);
            } else {
                this.dealCardsToPlayersAnimation(selectedCard, this.opponent, opponentActivecards, 'opponent', allCards.length);
            }
            playerScore.innerHTML = this.player.cards.length
            opponentScore.innerHTML = this.opponent.cards.length
        }, i +'0');
        players = [this.player, this.opponent];
    }
    
    dealCardsToPlayersAnimation(selectedCard, player, playerActive, animation, lastCard) {
        let lastCardFromDeck = lastCard +1;
        let witchPlayer = player;
        listCardsBeforeDealing.childNodes[lastCardFromDeck].classList.add('animation-go-to-' + animation)
        setTimeout( ()=> {
            listCardsBeforeDealing.childNodes[lastCardFromDeck].classList.remove('animation-go-to-' + animation)
            playerActive.appendChild(listCardsBeforeDealing.childNodes[lastCardFromDeck])
        }, 400);
        witchPlayer.cards.push(selectedCard)
    }
}