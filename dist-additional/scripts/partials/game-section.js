import Fight from '../fight.js'

const containerCardsBeforeDealing = document.getElementById('container-cards-before-dealing')
const fightButton = document.querySelector('.game-section-fight-button');

export default class gameSection {
    constructor() {
        this.fight = new Fight();
    }

    fightButtonListener(
        players,
        playerActivecards,
        opponentActivecards,
        playerScore,
        opponentScore
    ) {
        const fightContext = this.fight
        let listening = 'fight'

        fightButton.classList.add('game-section-fight-button-visible')
        containerCardsBeforeDealing.classList.add('game-section-cards-container-invisible')

        fightButton.addEventListener('click', function clickFightButton() {
            if (listening == 'fight') {
                listening = ''

                if (fightContext.cards(
                    players,
                    playerActivecards,
                    opponentActivecards,
                    listening,
                    playerScore,
                    opponentScore
                ) === 'playOff') {
                    setTimeout(()=> { listening = 'fight'}, 3400)
                }
                else {
                    setTimeout(()=> {listening = 'fight'}, 1700)
                }
            }
            else { return false }
        })
    }
}