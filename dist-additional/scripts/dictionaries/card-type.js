export const cardTypes = Object.freeze({
    as: 'As',
    krol: 'Krol',
    dama: 'Dama',
    walet: 'Walet',
    dziesięć: '10',
    dziewięć: '9',
    osiem: '8',
    siedem: '7',
    sześć: '6',
    pięć: '5',
    cztery: '4',
    trzy: '3',
    dwa: '2'
}) 